# QrcodeScanner

this is a simples project with jsqr library. I create a scanner to qrcode in angular.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Demo
<img src="https://cdn.discordapp.com/attachments/969237668426809464/1090016952572969020/image.png"/>
